<?php

namespace Collecton\AssoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PointDeCollecte
 *
 * @ORM\Table(name="point_de_collecte")
 * @ORM\Entity(repositoryClass="Collecton\AssoBundle\Repository\PointDeCollecteRepository")
 */
class PointDeCollecte
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_pt", type="string", length=255)
     */
    private $nomPt;

    /**
     * @var int
     *
     * @ORM\Column(name="id_asso", type="integer", length=255, nullable=true)
     */
    private $idAsso;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_asso", type="string", length=255)
     */
    private $nomAsso;

    /**
     * @var string
     *
     * @ORM\Column(name="type_pt", type="string", length=255)
     */
    private $typePt;

    /**
     * @var string
     *
     * @ORM\Column(name="type_objets", type="string", length=255)
     */
    private $typeObjets;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_objets", type="string", length=255)
     */
    private $catObjets;

    /**
     * @var string
     *
     * @ORM\Column(name="horaires", type="text", nullable=true)
     */
    private $horaires;

    /**
     * @var string
     *
     * @ORM\Column(name="date", type="text", nullable=true)
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     * @var float
     *
     * @ORM\Column(name="lat", type="float", nullable=true)
     */
    private $lat;

    /**
     * @var float
     *
     * @ORM\Column(name="lng", type="float", nullable=true)
     */
    private $lng;

    /**
     * @var string
     *
     * @ORM\Column(name="acces", type="string", length=255, nullable=true)
     */
    private $acces;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_mail", type="string", length=255)
     */
    private $contactMail;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_tel", type="string", length=255)
     */
    private $contactTel;

    /**
     * @var string
     *
     * @ORM\Column(name="site_web", type="string", length=255)
     */
    private $siteWeb;

    /**
     * @var string
     *
     * @ORM\Column(name="destination", type="text", nullable=true)
     */
    private $destination;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nomPt
     *
     * @param string $nomPt
     *
     * @return PointDeCollecte
     */
    public function setNomPt($nomPt)
    {
        $this->nomPt = $nomPt;

        return $this;
    }

    /**
     * Get nomPt
     *
     * @return string
     */
    public function getNomPt()
    {
        return $this->nomPt;
    }

    /**
     * Set idAsso
     *
     * @param string $idAsso
     *
     * @return PointDeCollecte
     */
    public function setIdAsso($idAsso)
    {
        $this->idAsso = $idAsso;

        return $this;
    }

    /**
     * Get idAsso
     *
     * @return string
     */
    public function getIdAsso()
    {
        return $this->idAsso;
    }

    /**
     * Set nomAsso
     *
     * @param string $nomAsso
     *
     * @return PointDeCollecte
     */
    public function setNomAsso($nomAsso)
    {
        $this->nomAsso = $nomAsso;

        return $this;
    }

    /**
     * Get nomAsso
     *
     * @return string
     */
    public function getNomAsso()
    {
        return $this->nomAsso;
    }

    /**
     * Set typePt
     *
     * @param string $typePt
     *
     * @return PointDeCollecte
     */
    public function setTypePt($typePt)
    {
        $this->typePt = $typePt;

        return $this;
    }

    /**
     * Get typePt
     *
     * @return string
     */
    public function getTypePt()
    {
        return $this->typePt;
    }

    /**
     * Set typeObjets
     *
     * @param string $typeObjets
     *
     * @return PointDeCollecte
     */
    public function setTypeObjets($typeObjets)
    {
        $this->typeObjets = $typeObjets;

        return $this;
    }

    /**
     * Get typeObjets
     *
     * @return string
     */
    public function getTypeObjets()
    {
        return $this->typeObjets;
    }

    /**
     * Set catObjets
     *
     * @param string $catObjets
     *
     * @return PointDeCollecte
     */
    public function setCatObjets($catObjets)
    {
        $this->catObjets = $catObjets;

        return $this;
    }

    /**
     * Get catObjets
     *
     * @return string
     */
    public function getCatObjets()
    {
        return $this->catObjets;
    }

    /**
     * Set horaires
     *
     * @param string $horaires
     *
     * @return PointDeCollecte
     */
    public function setHoraires($horaires)
    {
        $this->horaires = $horaires;

        return $this;
    }

    /**
     * Get horaires
     *
     * @return string
     */
    public function getHoraires()
    {
        return $this->horaires;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return PointDeCollecte
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return PointDeCollecte
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set lat
     *
     * @param float $lat
     *
     * @return PointDeCollecte
     */
    public function setLat($lat)
    {
        $this->lat = $lat;

        return $this;
    }

    /**
     * Get lat
     *
     * @return float
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Set lng
     *
     * @param float $lng
     *
     * @return PointDeCollecte
     */
    public function setLng($lng)
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * Get lng
     *
     * @return float
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * Set acces
     *
     * @param string $acces
     *
     * @return PointDeCollecte
     */
    public function setAcces($acces)
    {
        $this->acces = $acces;

        return $this;
    }

    /**
     * Get acces
     *
     * @return string
     */
    public function getAcces()
    {
        return $this->acces;
    }

    /**
     * Set contactMail
     *
     * @param string $contactMail
     *
     * @return PointDeCollecte
     */
    public function setContactMail($contactMail)
    {
        $this->contactMail = $contactMail;

        return $this;
    }

    /**
     * Get contactMail
     *
     * @return string
     */
    public function getContactMail()
    {
        return $this->contactMail;
    }

    /**
     * Set contactTel
     *
     * @param string $contactTel
     *
     * @return PointDeCollecte
     */
    public function setContactTel($contactTel)
    {
        $this->contactTel = $contactTel;

        return $this;
    }

    /**
     * Get contactTel
     *
     * @return string
     */
    public function getContactTel()
    {
        return $this->contactTel;
    }

    /**
     * Set siteWeb
     *
     * @param string $siteWeb
     *
     * @return PointDeCollecte
     */
    public function setSiteWeb($siteWeb)
    {
        $this->siteWeb = $siteWeb;

        return $this;
    }

    /**
     * Get siteWeb
     *
     * @return string
     */
    public function getSiteWeb()
    {
        return $this->siteWeb;
    }

    /**
     * Set destination
     *
     * @param string $destination
     *
     * @return PointDeCollecte
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    /**
     * Get destination
     *
     * @return string
     */
    public function getDestination()
    {
        return $this->destination;
    }
}
