<?php

namespace Collecton\AssoBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Collecton\AssoBundle\Entity\PointDeCollecte;
use Symfony\Component\HttpFoundation\Request;
use Collecton\AssoBundle\Form\PointDeCollecteType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Collecton\AssoBundle\Entity\Fichier;
use Collecton\AssoBundle\Form\FichierType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;




class AssoController extends Controller
{
    public function profilAction()
    {
        return $this->render('CollectonAssoBundle:Asso:profil.html.twig');
    }

    public function collecteAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $pt_de_co = new PointDeCollecte();
        $form_pt = $this->get('form.factory')->create(PointDeCollecteType::class, $pt_de_co);
        $form_pt->add('Valider', SubmitType::class);
        if ($request->isMethod('POST') && $form_pt->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($pt_de_co);
            $em->flush();
        }
        $fichier = new Fichier();
        $form_fichier = $this->get('form.factory')->create(FichierType::class, $fichier);
        $form_fichier->add('Valider', SubmitType::class);
        if ($request->isMethod('POST') && $form_fichier->handleRequest($request)->isValid()) {
            $fichier->upload();
            $fichier->setNom("test");
            $em = $this->getDoctrine()->getManager();
            $em->persist($fichier);
            $em->flush();
        }

        return $this->render('CollectonAssoBundle:Asso:collecte.html.twig', array(
            'form' => $form_pt->createView(),
            'form_file' => $form_fichier->createView()
        ));
    }

    public function actuAction()
    {
        return $this->render('CollectonAssoBundle:Asso:actu.html.twig');
    }


    public function downloadFileAction(){
        $response = new BinaryFileResponse('nouveauxpointsdecollecte.xlsx');
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'nouveauxpointsdecollecte.xlsx');
        return $response;
    }
}