<?php

namespace Collecton\AssoBundle\Form;

use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PointDeCollecteType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomAsso', TextType::class, array(
                'attr' =>array(
                ),
                'label' => "Collecte organisée par *"
            ))
            ->add('siteWeb', TextType::class, array(
                'attr' =>array(
                    'placeholder' => "http://monAssociation.org"
                ),
                'label' => "Site Internet"
            ))
            ->add('contactTel', TextType::class, array(
                'attr' =>array(
                    'placeholder' => "01 XX XX XX XX"
                ),
                    'label' => "Téléphone"
                ))
            ->add('contactMail', TextType::class, array(
                'attr' =>array(
                    'placeholder' => "monMail@monAsso.com"
                ),
                'label' => "Mail"
            ))
            ->add('nomPt', TextType::class, array(
                'attr' =>array(
                    'placeholder' => "Nom du point de collecte"
                ),
                'label' => "Nom du point de collecte *"
            ))
            ->add('adresse', TextType::class, array(
                'attr' =>array(
                    'placeholder' => "2 rue Victor Hugo, 75015 PARIS"
                ),
                'label' => "Adresse *"
            ))
            ->add('acces', TextareaType::class, array(
                'attr' =>array(
                    'placeholder' => "Informations particulières, transports à proximité..."
                ),
                'label' => "Accès"
            ))
            ->add('typePt',null, array(
                'attr' =>array(
                ),
                'label' => "La collecte est-elle ponctuelle (limitée dans le temps) ou permanente ? *"
            ))
            ->add('horaires', TextareaType::class, array(
                'attr' =>array(
                    'placeholder' => "Renseignez les horaires auxquels les donateurs pourront venir déposer leurs dons"
                ),
                'label' => "Horaires *"
            ))
            ->add('date', TextareaType::class, array(
                    'attr' =>array(
                        'placeholder' => "Renseignez les dates auxquelles les donateurs pourront venir déposer leurs dons"
                    ),
                'label' => "Dates *"
            ))
            ->add('catObjets', null, array(
                'attr' =>array(
                 ),
                'label' => "Sélectionnez la ou les catégories concernée(s) par la collecte *"
            ))
            ->add('typeObjets', TextareaType::class, array(
                'attr' =>array(
                    'placeholder' => "Vous pouvez y préciser le type d'objets que vous acceptez, leur état..."
                ),
                'label' => "Spécifiez le type d'objets collectés *"
            ))
            ->add('destination', TextareaType::class, array(
                'attr' =>array(
                    'placeholder' => "Informez les donateurs de ce à quoi serviront leurs dons !"
                ),
                'label' => "Que vont devenir les dons ? *"
            ))
        ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Collecton\AssoBundle\Entity\PointDeCollecte'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'collecton_assobundle_pointdecollecte';
    }


}
