<?php

namespace Collecton\PortalBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ContactEntreType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('nom',null, array(
        'attr' => array(
            'placeholder'   => "Nom de l'entreprise"
        )))
            ->add('email',EmailType::class, array(
                'attr' => array(
                    'placeholder'   => "Email"
                )))
            ->add('objet',null, array(
            'attr' =>array(
                'placeholder' => "Objet"
        )))
            ->add('message', TextareaType::class, array(
            'attr' =>array(
                'placeholder' => "Message"
        )))
            ->add('Envoyer', SubmitType::class);
        ;
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Collecton\PortalBundle\Entity\Contact'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'collecton_portalbundle_contact';
    }


}
