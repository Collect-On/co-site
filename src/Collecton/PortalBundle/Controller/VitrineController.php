<?php

namespace Collecton\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Collecton\PortalBundle\Entity\User;
use Collecton\PortalBundle\Entity\AdresseMaps;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
//use Collecton\PortalBundle\Form\InscriptionType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Collecton\PortalBundle\Entity\Mail;
use Collecton\PortalBundle\Entity\Contact;
use Collecton\PortalBundle\Form\ContactAssoType;
use Collecton\PortalBundle\Form\ContactEntreType;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Security;

class VitrineController extends Controller
{


    public function showAction(  Request $request)
    {
        $em = $this->getDoctrine()->getManager();



        //Pour récupérer l'user connecté
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirect( $this->generateUrl('login_check'));
        }
//////////////FORMULAIRE CONTACT ASSO \\\\\\\\\\\
        $contact = new Contact();
        $form_contact_asso = $this->get('form.factory')->create(ContactAssoType::class, $contact);
        if ($request->isMethod('POST') && $form_contact_asso->handleRequest($request)->isValid()) {

            $objet = 'Nouveau message de '.strval($contact->getNom()).' : '.strval($contact->getObjet());

            $message = (new \Swift_Message($objet))
                ->setFrom('pauline@collect-on.com')
                ->setTo('pauline.druesne@free.fr')
                ->setBody($contact->getMessage())
            ;
            $this->get('mailer')->send($message);
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
        }
        //////////////FORMULAIRE CONTACT ENTREPRISE \\\\\\\\\\\
        $contact = new Contact();
        $form_contact_entre = $this->get('form.factory')->create(ContactEntreType::class, $contact);
        if ($request->isMethod('POST') && $form_contact_entre->handleRequest($request)->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
        }

//////////////////FORMULAIRE MAILS\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\ pour la version prod
        $mail = new Mail();
        $form = $this->get('form.factory')->createBuilder(FormType::class, $mail)
            ->add('mail', TextType::class, [
                'attr' => [
                    'placeholder' => 'Email'
                ]])
            ->add('Valider', SubmitType::class)
            ->getForm();
        $today = getdate();



        if ($request->isMethod('POST')) {
            if ($form->handleRequest($request)->isValid())
            {
            if (!$em->getRepository('CollectonPortalBundle:Mail')->findOneBy(array('mail' => $mail->getMail()))) {
                $mail->setCreatedAt($today);
                $em->persist($mail);
                $em->flush();
                $request->getSession()->getFlashBag()->add('notice', 'Email bien enregistré.');
                return $this->render('CollectonPortalBundle:homepage:homepage.html.twig', [
                    'form' => $form->createView()
                ]);

                } else $request->getSession()->getFlashBag()->add('notice', 'Votre email est déjà enregistré.');
            }else $request->getSession()->getFlashBag()->add('notice', 'Email non valide.');
        }


        return $this->render('CollectonPortalBundle:homepage:homepage.html.twig', [
            'form_contact_asso' => $form_contact_asso->createView(),
            'form_contact_entre' => $form_contact_entre->createView(),
            'form' => $form->createView(),
            'today' => $today
        ]);
    }
}

