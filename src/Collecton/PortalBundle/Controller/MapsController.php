<?php

namespace Collecton\PortalBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Collecton\AssoBundle\Entity\PointDeCollecte;

class MapsController extends Controller
{
    public function MapSansCatAction(Request $request){
//        $encoders = array(new XmlEncoder(), new JsonEncoder());
//        $normalizers = array(new ObjectNormalizer());
//        $serializer = new Serializer($normalizers, $encoders);

        //on récupère toutes les adresses avec infos
        $xmlContent = null;
        $id = array();
        $id[0] = 0;
        $noms = array();
        $assos = array();
        $types_pt = array();
        $objets = array();
        $objets_cat = array();
        $horaires = array();
        $horaires_renseignement = array();
        $adresses = array();
        $lats = array();
        $lngs = array();
        $acces = array();
        $contacts_mail = array();
        $contacts_tel = array();
        $site_web = array();
        $cause = array();
        $destination = array();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('pointsdecollecte.xlsx');

        foreach ($phpExcelObject->setActiveSheetIndex(0)->getRowIterator() as $row) {

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

            foreach ($cellIterator as $cell) {

                if(end($id) < 283){
                    switch ($cell->getCoordinate()[0]){

                        case "B":
                            $id[] = end($id)+1;
                            $noms[] = $cell->getValue();
                            break;
                        case "C":
                            $assos[] = $cell->getValue();
                            break;
                        case "D":
                            $types_pt[] = $cell->getValue();
                            break;
                        case "E":
                            $objets[] = $cell->getValue();
                            break;
                        case "F":
                            $objets_cat[] = $cell->getValue();
                            break;
                        case "G":
                            $horaires[] = $cell->getValue();
                            break;
                        case "H":
                            $horaires_renseignement[] = $cell->getValue();
                            break;
                        case "I":
                            $adresses[] = $cell->getValue();
                            break;
                        case "J":
                            $lats[] = floatval($cell->getValue());
                            break;
                        case "K":
                            $lngs[] = floatval($cell->getValue());
                            break;
                        case "L":
                            $acces[] = $cell->getValue();
                            break;
                        case "M":
                            $contacts_mail[] = $cell->getValue();
                            break;
                        case "N":
                            $contacts_tel[] = $cell->getValue();
                            break;
                        case "O":
                            $site_web[] = $cell->getValue();
                            break;
                        case "P":
                            $cause[] = $cell->getValue();
                            break;
                        case "Q":
                            $destination[] = $cell->getValue();
                            break;

                    }

                }

            }
        }

        return new JsonResponse(array(
            'nb_pts' => sizeof($noms),
            'id' => $id,
            'noms'=> $noms,
            'assos' => $assos,
            'types_pt' => $types_pt,
            'objets' => $objets,
            'objets_cat' => $objets_cat,
            'horaires' => $horaires,
            'horaires_renseignement' => $horaires_renseignement,
            'adresses' => $adresses,
            'lats' => $lats,
            'lngs' => $lngs,
            'acces' => $acces,
            'contacts_mail' => $contacts_mail,
            'contacts_tel' => $contacts_tel,
            'site_web' => $site_web,
            'cause' => $cause,
            'destination' => $destination
        ));




    }

    public function MapAvecCatAction(Request $request){
//        $encoders = array(new XmlEncoder(), new JsonEncoder());
//        $normalizers = array(new ObjectNormalizer());
//        $serializer = new Serializer($normalizers, $encoders);

        //on récupère toutes les adresses avec infos
        $cat = $request->request->get('cat');
        $xmlContent = null;
        $id = array();
        $id[0] = 0;
        $noms = array();
        $assos = array();
        $types_pt = array();
        $objets = array();
        $objets_cat = array();
        $horaires = array();
        $horaires_renseignement = array();
        $adresses = array();
        $lats = array();
        $lngs = array();
        $acces = array();
        $contacts_mail = array();
        $contacts_tel = array();
        $site_web = array();
        $cause = array();
        $destination = array();
        $poubelle = array();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('pointsdecollecte.xlsx');

            foreach ($phpExcelObject->setActiveSheetIndex(0)->getRowIterator() as $row) {

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

                foreach ($cellIterator as $cell) {
                    if(end($id) < 283) {

                        switch ($cell->getCoordinate()[0]){
                            case "B":
                                $id[] = end($id)+1;
                                $noms[] = $cell->getValue();
                                break;
                            case "C":
                                $assos[] = $cell->getValue();
                                break;
                            case "D":
                                $types_pt[] = $cell->getValue();
                                break;
                            case "E":
                                $objets[] = $cell->getValue();
                                break;
                            case "F":
                                $objets_cat[] = $cell->getValue();
                                break;
                            case "G":
                                $horaires[] = $cell->getValue();
                                break;
                            case "H":
                                $horaires_renseignement[] = $cell->getValue();
                                break;
                            case "I":
                                $adresses[] = $cell->getValue();
                                break;
                            case "J":
                                $lats[] = floatval($cell->getValue());
                                break;
                            case "K":
                                $lngs[] = floatval($cell->getValue());
                                break;
                            case "L":
                                $acces[] = $cell->getValue();
                                break;
                            case "M":
                                $contacts_mail[] = $cell->getValue();
                                break;
                            case "N":
                                $contacts_tel[] = $cell->getValue();
                                break;
                            case "O":
                                $site_web[] = $cell->getValue();
                                break;
                            case "P":
                                $cause[] = $cell->getValue();
                                break;
                            case "Q":
                                $destination[] = $cell->getValue();
                                break;

                        }

                    }

                }
            }
            if(($cat != null) and ($cat != "Tout")) {
                $poub = $this->checkCat($cat, $objets_cat, $noms);

                    $id = $this->Nettoyage($poub, $id);
                    $noms = $this->Nettoyage($poub, $noms);
                    $assos = $this->Nettoyage($poub, $assos);
                    $types_pt = $this->Nettoyage($poub, $types_pt);
                    $objets = $this->Nettoyage($poub, $objets);
                    $objets_cat = $this->Nettoyage($poub, $objets_cat);
                    $horaires = $this->Nettoyage($poub, $horaires);
                    $horaires_renseignement = $this->Nettoyage($poub, $horaires_renseignement);
                    $adresses = $this->Nettoyage($poub, $adresses);
                    $lats = $this->Nettoyage($poub, $lats);
                    $lngs = $this->Nettoyage($poub, $lngs);
                    $acces = $this->Nettoyage($poub, $acces);
                    $contacts_mail = $this->Nettoyage($poub, $contacts_mail);
                    $contacts_tel = $this->Nettoyage($poub, $contacts_tel);
                    $site_web = $this->Nettoyage($poub, $site_web);
                    $cause = $this->Nettoyage($poub, $cause);
                    $destination = $this->Nettoyage($poub, $destination);


            }




        return new JsonResponse(array(
            'nb_pts' => sizeof($noms),
            'id' => $id,
            'noms'=> $noms,
            'assos' => $assos,
            'types_pt' => $types_pt,
            'objets' => $objets,
            'objets_cat' => $objets_cat,
            'horaires' => $horaires,
            'horaires_renseignement' => $horaires_renseignement,
            'adresses' => $adresses,
            'lats' => $lats,
            'lngs' => $lngs,
            'acces' => $acces,
            'contacts_mail' => $contacts_mail,
            'contacts_tel' => $contacts_tel,
            'site_web' => $site_web,
            'cause' => $cause,
            'destination' => $destination,
            'cat' => $cat,
            

        ));




    }

    public function checkCat($cat,  $objets_cat, $noms){
        $poubelle = array();
        for( $i = 0; $i<count($objets_cat); $i++){
            if($objets_cat[$i] == $cat){
                $poubelle[] = $i;
            }
        }
        return $poubelle;
    }

    public function Nettoyage($index, $array)
    {

        $temp = array();
        $i = 0;
        $a = 0;
        while (($i < count($index))and($a < count($array)) ){
                if ($a == $index[$i]) {
                    $temp [] = $array[$a];
                    $i++;
                    $a = 0;

                } else $a++;
        }
        return $temp;
    }

    public function MapAvecAssoAction(Request $request){

        //on récupère toutes les adresses avec infos
        $asso = $this->getUser()->getNomStructure();
        $xmlContent = null;
        $id = array();
        $id[0] = 0;
        $noms = array();
        $assos = array();
        $types_pt = array();
        $objets = array();
        $objets_cat = array();
        $horaires = array();
        $horaires_renseignement = array();
        $adresses = array();
        $lats = array();
        $lngs = array();
        $acces = array();
        $contacts_mail = array();
        $contacts_tel = array();
        $site_web = array();
        $cause = array();
        $destination = array();
        $poubelle = array();

        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('pointsdecollecte.xlsx');

        foreach ($phpExcelObject->setActiveSheetIndex(0)->getRowIterator() as $row) {

            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

            foreach ($cellIterator as $cell) {
                if(end($id) < 278) {

                    switch ($cell->getCoordinate()[0]){
                        case "B":
                            $id[] = end($id)+1;
                            $noms[] = $cell->getValue();
                            break;
                        case "C":
                            $assos[] = $cell->getValue();
                            break;
                        case "D":
                            $types_pt[] = $cell->getValue();
                            break;
                        case "E":
                            $objets[] = $cell->getValue();
                            break;
                        case "F":
                            $objets_cat[] = $cell->getValue();
                            break;
                        case "G":
                            $horaires[] = $cell->getValue();
                            break;
                        case "H":
                            $horaires_renseignement[] = $cell->getValue();
                            break;
                        case "I":
                            $adresses[] = $cell->getValue();
                            break;
                        case "J":
                            $lats[] = floatval($cell->getValue());
                            break;
                        case "K":
                            $lngs[] = floatval($cell->getValue());
                            break;
                        case "L":
                            $acces[] = $cell->getValue();
                            break;
                        case "M":
                            $contacts_mail[] = $cell->getValue();
                            break;
                        case "N":
                            $contacts_tel[] = $cell->getValue();
                            break;
                        case "O":
                            $site_web[] = $cell->getValue();
                            break;
                        case "P":
                            $cause[] = $cell->getValue();
                            break;
                        case "Q":
                            $destination[] = $cell->getValue();
                            break;

                    }

                }

            }
        }
        if(file_exists('uploads/excel/nouveauxpoints.xlsx')){
        $phpExcelObject = $this->get('phpexcel')->createPHPExcelObject('uploads/excel/nouveauxpoints.xlsx');

            foreach ($phpExcelObject->setActiveSheetIndex(0)->getRowIterator() as $row) {

                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set

                foreach ($cellIterator as $cell) {
                    if (end($id) < 300) {

                        switch ($cell->getCoordinate()[0]) {
                            case "B":
                                $id[] = end($id) + 1;
                                $noms[] = $cell->getValue();
                                break;
                            case "C":
                                $assos[] = $cell->getValue();
                                break;
                            case "D":
                                $types_pt[] = $cell->getValue();
                                break;
                            case "E":
                                $objets[] = $cell->getValue();
                                break;
                            case "F":
                                $objets_cat[] = $cell->getValue();
                                break;
                            case "G":
                                $horaires[] = $cell->getValue();
                                break;
                            case "H":
                                $horaires_renseignement[] = $cell->getValue();
                                break;
                            case "I":
                                $adresses[] = $cell->getValue();
                                break;
                            case "J":
                                $lats[] = floatval($cell->getValue());
                                break;
                            case "K":
                                $lngs[] = floatval($cell->getValue());
                                break;
                            case "L":
                                $acces[] = $cell->getValue();
                                break;
                            case "M":
                                $contacts_mail[] = $cell->getValue();
                                break;
                            case "N":
                                $contacts_tel[] = $cell->getValue();
                                break;
                            case "O":
                                $site_web[] = $cell->getValue();
                                break;
                            case "P":
                                $cause[] = $cell->getValue();
                                break;
                            case "Q":
                                $destination[] = $cell->getValue();
                                break;

                        }

                    }

                }
            }
        }
//        $id_b = array();
//        $id_b[0] = 0;
//        $noms_b = array();
//        $assos_b = array();
//        $types_pt_b = array();
//        $objets_b = array();
//        $objets_cat_b = array();
//        $horaires_b = array();
//        $adresses_b = array();
//        $acces_b = array();
//        $contacts_mail_b = array();
//        $contacts_tel_b = array();
//        $site_web_b = array();
//        $destination_b = array();
//
//        $em = $this->getDoctrine()->getManager();
//        $pt_b = $em->getRepository('CollectonAssoBundle:PointDeCollecte')->findAll();
////
//            $noms_b [] = $pt_b[0]->getNomPt();
//            $assos_b [] = $pt_b[0]->getNomAsso();
//            $types_pt_b [] = $pt_b[0]->getTypePt();
//            $objets_b [] = $pt_b[0]->getTypeObjets();
//            $objets_cat_b [] = $pt_b[0]->getCatObjets();
//            $horaires_b [] = $pt_b[0]->getHoraires();
//            $adresses_b [] = $pt_b[0]->getAdresse();
//            $acces_b [] = $pt_b[0]->getAcces();
//            $contacts_mail_b []= $pt_b[0]->getContactMail();
//            $contacts_tel_b [] = $pt_b[0]->getContactTel();
//            $site_web_b [] = $pt_b[0]->getSiteWeb();
//            $destination_b [] = $pt_b[0]->getDestination();



            $poub = $this->checkCat($asso, $assos, $noms);

            $id = $this->Nettoyage($poub, $id);
            $noms = $this->Nettoyage($poub, $noms);
            $assos = $this->Nettoyage($poub, $assos);
            $types_pt = $this->Nettoyage($poub, $types_pt);
            $objets = $this->Nettoyage($poub, $objets);
            $objets_cat = $this->Nettoyage($poub, $objets_cat);
            $horaires = $this->Nettoyage($poub, $horaires);
            $horaires_renseignement = $this->Nettoyage($poub, $horaires_renseignement);
            $adresses = $this->Nettoyage($poub, $adresses);
            $lats = $this->Nettoyage($poub, $lats);
            $lngs = $this->Nettoyage($poub, $lngs);
            $acces = $this->Nettoyage($poub, $acces);
            $contacts_mail = $this->Nettoyage($poub, $contacts_mail);
            $contacts_tel = $this->Nettoyage($poub, $contacts_tel);
            $site_web = $this->Nettoyage($poub, $site_web);
            $cause = $this->Nettoyage($poub, $cause);
            $destination = $this->Nettoyage($poub, $destination);






        return new JsonResponse(array(
            'excel' => array(
                'nb_pts' => sizeof($noms),
                'id' => $id,
                'noms'=> $noms,
                'assos' => $assos,
                'types_pt' => $types_pt,
                'objets' => $objets,
                'objets_cat' => $objets_cat,
                'horaires' => $horaires,
                'horaires_renseignement' => $horaires_renseignement,
                'adresses' => $adresses,
                'lats' => $lats,
                'lngs' => $lngs,
                'acces' => $acces,
                'contacts_mail' => $contacts_mail,
                'contacts_tel' => $contacts_tel,
                'site_web' => $site_web,
                'cause' => $cause,
                'destination' => $destination,
                'asso' => $asso),
//             'bdd'=> array(
//                 'nb_pts' => sizeof($noms_b),
//                 'id' => $id_b,
//                 'noms'=> $noms_b,
//                 'assos' => $assos_b,
//                 'types_pt' => $types_pt_b,
//                 'objets' => $objets_b,
//                 'objets_cat' => $objets_cat_b,
//                 'horaires' => $horaires_b,
//                 'adresses' => $adresses_b,
//                 'acces' => $acces_b,
//                 'contacts_mail' => $contacts_mail_b,
//                 'contacts_tel' => $contacts_tel_b,
//                 'site_web' => $site_web_b,
//                 'destination' => $destination_b


             )


        );




    }

    public function ExcelObj(){
        return $this->get('phpexcel')->createPHPExcelObject('pointsdecollecte.xlsx');
    }

}