<?php
/**
 * Created by PhpStorm.
 * User: begui
 * Date: 22/01/2019
 * Time: 22:40
 */

namespace Collecton\UserBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class EntrepriseType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom_structure', null, array(
                'label' => "Entreprise",
                'attr' => array(
                    'placeholder'   => "Nom de l'entreprise"
                )
            ))
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => "Saisissez une brève description de votre entreprise et de ce que vous entreprenez/comptez entreprendre dans le domaine du don d'objet"
                )
            ))
            ->add('contact_prenom', null, array(
                'attr' => array(
                    'placeholder' => "Prénom"
                )
            ))
            ->add('contact_nom', null, array(
                'attr' => array(
                    'placeholder' => "Nom"
                )
            ))
            ->add('contact_statut', null, array(
                'attr' => array(
                    'placeholder' => "Statut dans l'entreprise"
                )
            ))
            ->add('contact_tel', null, array(
                'attr' => array(
                    'placeholder' => "Téléphone"
                )
            ))
        ;
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Collecton\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'collecton_userbundle_user';
    }
}