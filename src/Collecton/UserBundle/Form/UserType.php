<?php

namespace Collecton\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type',ChoiceType::class, [
                'choices' => [
                    'Association' => 'Association',
                    'Entreprise' => 'Entreprise'
                ],
                'label' => "Vous êtes :"
            ])
            ->add('username', EmailType::class, array(
            'attr' =>array(
                'placeholder' => "Email"
            )
    ))
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'attr' =>array(
                    'placeholder' => "Mot de passe"
                ),
                'first_options' => array('label' => 'form.password', 'attr' =>array(
                    'placeholder' => "Mot de passe"
                ),),
                'second_options' => array('label' => 'form.password_confirmation', 'attr' =>array(
                    'placeholder' => "Confirmer le mot de passe"
                ),)
            ))
           ;
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Collecton\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'collecton_userbundle_user';
    }


}
