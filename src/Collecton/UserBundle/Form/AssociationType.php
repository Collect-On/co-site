<?php

namespace Collecton\UserBundle\Form;

use Doctrine\DBAL\Types\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class AssociationType extends AbstractType{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom_structure', null, array(
                'label'         => "Association",
                'attr' => array(
                    'placeholder'   => "Nom de l'association"
                )
            ))
            ->add('description', TextareaType::class, array(
                'attr' => array(
                    'placeholder' => "Saisissez une brève description de votre association et de vos activités"
                )
            ))
            ->add('contact_prenom', null, array(
                'attr' => array(
                    'placeholder' => "Prénom"
                )
            ))
            ->add('contact_nom', null, array(
                'attr' => array(
                    'placeholder' => "Nom"
                )
            ))
            ->add('contact_statut', null, array(
                'attr' => array(
                    'placeholder' => "Statut dans l'association"
                )
            ))
            ->add('contact_tel', null, array(
                'attr' => array(
                    'placeholder' => "Téléphone"
                )
            ))
            ;
    }/**
 * {@inheritdoc}
 */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Collecton\UserBundle\Entity\User'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'collecton_userbundle_user';
    }
}