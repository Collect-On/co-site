<?php

namespace Collecton\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="Collecton\UserBundle\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="array")
     */
    private $roles;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_structure", type="string", nullable=true)
     */
    private $nom_structure;

    /**
     * @var text
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_prenom", type="string", nullable=true)
     */
    private $contact_prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_nom", type="string", nullable=true)
     */
    private $contact_nom;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_statut", type="string", nullable=true)
     */
    private $contact_statut;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_tel", type="string", nullable=true)
     */
    private $contact_tel;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set roles
     *
     * @param array $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    public function eraseCredentials()
    {

    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return User
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nomStructure
     *
     * @param string $nomStructure
     *
     * @return User
     */
    public function setNomStructure($nomStructure)
    {
        $this->nom_structure = $nomStructure;

        return $this;
    }

    /**
     * Get nomStructure
     *
     * @return string
     */
    public function getNomStructure()
    {
        return $this->nom_structure;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set contactPrenom
     *
     * @param string $contactPrenom
     *
     * @return User
     */
    public function setContactPrenom($contactPrenom)
    {
        $this->contact_prenom = $contactPrenom;

        return $this;
    }

    /**
     * Get contactPrenom
     *
     * @return string
     */
    public function getContactPrenom()
    {
        return $this->contact_prenom;
    }

    /**
     * Set contactNom
     *
     * @param string $contactNom
     *
     * @return User
     */
    public function setContactNom($contactNom)
    {
        $this->contact_nom = $contactNom;

        return $this;
    }

    /**
     * Get contactNom
     *
     * @return string
     */
    public function getContactNom()
    {
        return $this->contact_nom;
    }

    /**
     * Set contactStatut
     *
     * @param string $contactStatut
     *
     * @return User
     */
    public function setContactStatut($contactStatut)
    {
        $this->contact_statut = $contactStatut;

        return $this;
    }

    /**
     * Get contactStatut
     *
     * @return string
     */
    public function getContactStatut()
    {
        return $this->contact_statut;
    }

    /**
     * Set contactTel
     *
     * @param string $contactTel
     *
     * @return User
     */
    public function setContactTel($contactTel)
    {
        $this->contact_tel = $contactTel;

        return $this;
    }

    /**
     * Get contactTel
     *
     * @return string
     */
    public function getContactTel()
    {
        return $this->contact_tel;
    }
}
