<?php


namespace Collecton\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class SecurityController extends Controller

{

    public function loginAction()
    {

        // Si le visiteur est déjà identifié, on le redirige vers l'accueil
        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $user_id = $this->container->get('security.context')->getToken()->getUser()->getCandidat()->getId();
//            return $this->redirectToRoute('collecton_portal_homepage');
        }


        // Le service authentication_utils permet de récupérer le nom d'utilisateur
        // et l'erreur dans le cas où le formulaire a déjà été soumis mais était invalide
        // (mauvais mot de passe par exemple)
        $authenticationUtils = $this->get('security.authentication_utils');

        if($authenticationUtils->getLastAuthenticationError() ){
            $this->badLoginAction();
            return $this->redirect( $this->generateUrl('bad_login'));
        }else {
        return $this->render('CollectonUserBundle:Security:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),

        ));}

    }

    public function loginCheckAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $user = $authenticationUtils->getLastUsername();
//        if ($this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
//            $user = $this->getUser();
//        }
        return $this->render('CollectonUserBundle:Security:login_check.html.twig');
    }

    public function badLoginAction()
    {
        return $this->render('CollectonUserBundle:Security:bad_login.html.twig');
    }

    public function testLoginAction()
    {
        return $this->render('CollectonUserBundle:Security:test_login.html.twig');
    }
    public function loginAfterAction()
    {
        return $this->render('CollectonUserBundle:Security:test_login.html.twig');
    }

}