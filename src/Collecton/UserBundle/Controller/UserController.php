<?php


namespace Collecton\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


class UserController extends Controller

{

    public function userCoAction()
    {

        return new JsonResponse(array(
            'user' => $this->getUser()->getNomStructure()
        ));
    }
}
