<?php

namespace Collecton\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Collecton\UserBundle\Entity\User;
use Collecton\UserBundle\Form\UserType;
use Collecton\UserBundle\Form\AssociationType;
use Collecton\UserBundle\Form\EntrepriseType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class RegisterController extends Controller

{

    public function registerAction(Request $request)
    {
        $user = new User;
        $inscription = $this->get('form.factory')->create(UserType::class, $user);
        $inscription->add('envoyer', SubmitType::class);

        if ($request->isMethod('POST') && $inscription->handleRequest($request)->isValid()) {
            //$message = (new \Swift_Message('Hello Email'))
              //  ->setFrom('pauline@collect-on.com')
               // ->setTo('pauline.druesne@free.fr')
                //->setBody('test')
            //;

//            $mailer->send($message);

            // or, you can also fetch the mailer service this way
            // $this->get('mailer')->send($message);


            $password = $user->getPassword();
            $user->setSalt('');
          $user->setType('Association');
            $user->setRoles(array('ROLES_USER'));
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $user->setPassword($encoder->encodePassword($password, $user->getSalt()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('register_check', array('userid' => $user->getId()));

        }
        return $this->render('CollectonUserBundle:Register:register.html.twig', array(
            'inscription' => $inscription->createView()
        ));

    }

    public function registerCheckAction(Request $request, $userid){

        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository('CollectonUserBundle:User')->find($userid);

        //If asso
        if($user->getType()==='Association')
        $form = $this->get('form.factory')->create(AssociationType::class, $user);
        //If entreprise
        if($user->getType()==='Entreprise')
        $form = $this->get('form.factory')->create(EntrepriseType::class, $user);


        $form->add('VALIDER', SubmitType::class);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) {

            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('login_apres_inscription');

        }

        return $this->render('CollectonUserBundle:Register:register_check.html.twig', array(

            'form' => $form->createView(),
            'user' => $user->getUsername()

        ));
    }
}