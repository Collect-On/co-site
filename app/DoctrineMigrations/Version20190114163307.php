<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190114163307 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE feuil1');
        $this->addSql('ALTER TABLE user ADD type INT NOT NULL, ADD mail VARCHAR(255) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE feuil1 (A VARCHAR(41) DEFAULT NULL COLLATE utf8_general_ci, B VARCHAR(54) DEFAULT NULL COLLATE utf8_general_ci, C VARCHAR(17) DEFAULT NULL COLLATE utf8_general_ci, D VARCHAR(18) DEFAULT NULL COLLATE utf8_general_ci, E VARCHAR(4) DEFAULT NULL COLLATE utf8_general_ci) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE user DROP type, DROP mail');
    }
}
