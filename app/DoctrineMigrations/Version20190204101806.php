<?php

namespace Application\Migrations;

use Collecton\AssoBundle\Entity\PointDeCollecte;
use Collecton\PortalBundle\CollectonPortalBundle;
use Collecton\PortalBundle\Controller\MapsController;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PHPExcel;
use PHPExcel_IOFactory;



/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190204101806 extends AbstractMigration implements ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE point_de_collecte (id INT AUTO_INCREMENT NOT NULL, nom_pt VARCHAR(255) NOT NULL, id_asso INT DEFAULT NULL, nom_asso VARCHAR(255) NOT NULL, type_pt VARCHAR(255) NOT NULL, type_objets VARCHAR(255) NOT NULL, cat_objets VARCHAR(255) NOT NULL, horaires LONGTEXT DEFAULT NULL, date LONGTEXT DEFAULT NULL, adresse VARCHAR(255) NOT NULL, lat DOUBLE PRECISION DEFAULT NULL, lng DOUBLE PRECISION DEFAULT NULL, acces VARCHAR(255) DEFAULT NULL, contact_mail VARCHAR(255) NOT NULL, contact_tel VARCHAR(255) NOT NULL, site_web VARCHAR(255) NOT NULL, destination LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE point_de_collecte');
    }

    public function postUp(Schema $schema){



        $em = $this->container->get('doctrine.orm.entity_manager');
        $id = array();
        $id[0] = 0;

        $objPHPExcel = PHPExcel_IOFactory::load("../pointsdecollecte.xlsx");

        foreach ($objPHPExcel->setActiveSheetIndex(0)->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
            foreach ($cellIterator as $cell) {

                if(end($id) < 283){
                    $pt_de_co = new PointDeCollecte();
                    switch ($cell->getCoordinate()[0]){

                        case "B":
                            $id[] = end($id)+1;
                            $pt_de_co->setNomPt($cell->getValue());
                            break;
                        case "C":
                            $pt_de_co->setNomAsso($cell->getValue());
                            break;
                        case "D":
                            $pt_de_co->setTypePt($cell->getValue());
                            break;
                        case "E":
                            $pt_de_co->setTypeObjets($cell->getValue());
                            break;
                        case "F":
                            $pt_de_co->setCatObjets($cell->getValue());
                            break;
                        case "G":
                            $pt_de_co->setHoraires($cell->getValue());
                            break;
                        case "I":
                            $pt_de_co->setAdresse($cell->getValue());
                            break;
                        case "J":
                            $pt_de_co->setLat(floatval($cell->getValue()));
                            break;
                        case "K":
                            $pt_de_co->setLng(floatval($cell->getValue()));
                            break;
                        case "L":
                            $pt_de_co->setAcces($cell->getValue());
                            break;
                        case "M":
                            $pt_de_co->setContactMail($cell->getValue());
                            break;
                        case "N":
                            $pt_de_co->setContactTel($cell->getValue());
                            break;
                        case "O":
                            $pt_de_co->setSiteWeb($cell->getValue());
                            break;
                        case "Q":
                            $pt_de_co->setDestination($cell->getValue());
                            break;

                    }
                    $em->persist($pt_de_co);
                    $em->flush();

                }

            }
        }

    }

}
